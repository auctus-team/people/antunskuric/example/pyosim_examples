from pyosim.OsimModel import OsimModel

## uncomment for different models
# one arm
model_path = './opensim_models/upper_body/unimanual/MoBL-ARMS Upper Extremity Model/MOBL_ARMS_fixed_41.osim'
# model_path = './opensim_models/upper_body/unimanual/arm26.osim'
# model_path = './opensim_models/upper_body/unimanual/OSarm412.osim'
# model_path = './opensim_models/upper_body/unimanual/Wu_Shoulder_Model.osim'

# both arms
# model_path = './opensim_models/upper_body/bimanual/MoBL_ARMS_bimanual_6_2_21.osim'
# model_path = './opensim_models/upper_body/bimanual/full_upper_body_marks.osim'

model = OsimModel(model_path,visualize=True)
# set model to neutral or random state
model.setToRandom()
# model.setToNeutral()

#print different model variables
model.printBodiesPositions()
model.printMarkersPositions()
model.printJointsValues()

# print model jacobian
print(model.getJacobian(n=11))

# display model
model.displayModel()
