# PyOsim examples

If you have already installed the OpenSim with python API then you just clone this repository to your PC. 

> Make sure to clone the submodules as well

```bash
git clone --recurse-submodules git@gitlab.inria.fr:askuric/pyosim_examples.git
cd pyosim_examples
git submodule update --remote
```



## Opensim using conda

### First build conda package for open-sim:
https://github.com/opensim-org/conda-opensim/tree/use_opensim43

```
git clone https://github.com/opensim-org/conda-opensim.git
cd conda-opensim
conda build opensim
```

### Install anaconda environment 

```
cd pyosim
conda env create -f env.yaml
conda activate opensim
```

### Install opensim anaconda package
```
conda install --use-local opensim
```
